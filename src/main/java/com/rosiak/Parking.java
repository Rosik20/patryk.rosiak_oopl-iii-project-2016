package com.rosiak;

import java.io.*;
import java.util.*;
/**
 * Created by Rosiak on 22.01.2017.
 */
public class Parking {
    public static int menuParking(){
        System.out.println();
        System.out.println("            Parking");
        System.out.println("     1.Brama");
        System.out.println("     2.Gdzie jest samochod ");
        System.out.println("     3.Zarzadzanie ");
        System.out.println("     0.Koniec");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }
    public static int menuBrama(){
        System.out.println();
        System.out.println("          Brama");
        System.out.println("     1.Wjazd ");
        System.out.println("     2.Wyjazd ");
        System.out.println("     0.Wstecz ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }
    public static int menuZarzadzanie(){
        System.out.println();
        System.out.println("           Zarzadzanie");
        System.out.println("     1.Pracownicy");
        System.out.println("     2.Stan ");
        System.out.println("     0.Wstecz ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();//w;return w;
    }
    public static int menuPracownicy(){
        System.out.println();
        System.out.println("               Pracownicy");
        System.out.println("     1.Dodaj");
        System.out.println("     2.Usun ");
        System.out.println("     3.Wyswietl ");
        System.out.println("     0.Wstecz ");
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }
    public static String wprowadzDane(){
        Scanner input = new Scanner(System.in);
        System.out.println("Imie ");
        String name = input.nextLine();
        System.out.println("Nazwisko ");
        String surname = input.nextLine();
        return name + " " + surname;
    }
    public static void serialized(Map employees){
        try
        {
            FileOutputStream fos = new FileOutputStream("hashmap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(employees);
            oos.close();
            fos.close();
            System.out.printf("HashMap zapisano w hashmap.ser");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args){
        //////////////////////////////////////////////////////////////////////////////////
        Map<String , Boolean> pracownicy;
        Map<String , Integer> map= new HashMap<>();

        try
        {
            FileInputStream fis = new FileInputStream("hashmap.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            pracownicy = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("nie znaleziono");
            c.printStackTrace();
            return ;
        }
        System.out.println("Odczytano HashMap..");
        int n = 5;
        Set<Map.Entry<String, Boolean>> prefe = pracownicy.entrySet();
        for (Map.Entry<String, Boolean> pref: prefe) {
            if( pref.getValue()){
                n++;
            }
        }

        Miejsce[] miejsce = new Miejsce[20];
        for(int i = 0 ; i < n ; i++){
            miejsce[i] = new Uprzywilejowane();
        }
        for(int i = n ; i < 20; i++){
            miejsce[i] = new Zwykle();
        }

        Scanner input = new Scanner(System.in);
        int menu = menuParking() ;
        while(menu!=0) {
            switch (menu) {
                case 1:
                    int menuBrama = menuBrama();
                    while (menuBrama != 0) {
                        switch (menuBrama) {
                            case 1:
                                String dane =wprowadzDane();
                                if (pracownicy.containsKey(dane)) {
                                    if(pracownicy.get(dane)){
                                        for(int i = 0 ; i < n ; i++){
                                            if(!miejsce[i].getStan()){
                                                miejsce[i].wjazd(dane);
                                                miejsce[i].wj();
                                                map.put(dane , i);
                                                break;
                                            }
                                        }
                                    }
                                    else{
                                        for(int i = n ; i < 20 ; i++){
                                            if(!miejsce[i].getStan()){
                                                miejsce[i].wjazd(dane);
                                                miejsce[i].wj();
                                                map.put(dane , i);
                                                break;
                                            }
                                            else {
                                                if( i == 19){
                                                    System.out.println("Brak miejsc");
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    System.out.println("Nie ma takiego pracownika");
                                }
                                break;
                            case 2:
                                dane =wprowadzDane();
                                if(map.containsKey(dane)){
                                    miejsce[map.get(dane)].wyjazd(dane);
                                    miejsce[map.get((dane))].wy();
                                    map.remove(dane);
                                }
                                else {
                                    System.out.println("Nie ma takiego pracownika");
                                }

                                break;
                            default:
                                System.out.println("Zly numer");
                        }
                        menuBrama = menuBrama();
                    }
                    break;
                case 2:
                    String dane = wprowadzDane();
                    if(map.containsKey(dane)) {
                        System.out.println("Samochod na miejscu nr." + (map.get(dane) + 1));
                    }
                    else {
                        System.out.println("Brak samochodu na parkingu");
                    }
                    break;
                case 3:
                    int menuZarzadzanie = menuZarzadzanie();
                    while (menuZarzadzanie != 0) {
                        switch (menuZarzadzanie) {
                            case 1:
                                int menuPracownicy = menuPracownicy();
                                while (menuPracownicy != 0) {
                                    switch (menuPracownicy) {
                                        case 1:
                                            dane = wprowadzDane();
                                            if (pracownicy.containsKey(dane)) {
                                                System.out.println("Pracownik istnieje");
                                            }
                                            else {
                                                System.out.println("Wprowadz preferencje(true/false) ");
                                                boolean preference = input.nextBoolean();
                                                pracownicy.put(dane, preference);
                                                System.out.println("Pracownik dodany ");
                                            }
                                            break;
                                        case 2:
                                            dane = wprowadzDane();
                                            if (pracownicy.containsKey(dane)) {
                                                pracownicy.remove(dane);
                                                System.out.println("Pracownik usuniety ");
                                            }
                                            else {
                                                System.out.println("Nie ma takiego pracownika");
                                            }
                                            break;
                                        case 3:
                                            Set<Map.Entry<String, Boolean>> pracownikk = pracownicy.entrySet();
                                            for (Map.Entry<String, Boolean> pracownik: pracownikk) {
                                                System.out.print("Dane: " + pracownik.getKey() + "  || " + " uprzywilejowanie: " + pracownik.getValue());
                                                System.out.println();
                                            }
                                            break;
                                        default:
                                            System.out.println("zly numer");
                                    }
                                    menuPracownicy = menuPracownicy();
                                }
                                break;
                            case 2:
                                for(Miejsce x:miejsce){
                                    System.out.println(x);
                                }
                                break;
                            default:
                                System.out.println("zly numer");
                        }
                        menuZarzadzanie = menuZarzadzanie();
                    }
                    break;
                default:
                    System.out.println("zly numer");
            }
            menu = menuParking();
        }
        serialized(pracownicy);
    }
}
