package com.rosiak;

/**
 * Created by Rosiak on 22.01.2017.
 */
public abstract class Miejsce implements Cos1 , Cos2{
    private int id;
    private static  int nextId;
    private boolean stan; // 1-zajety 0-wolny
    private boolean uprzywilejownaie;
    private String kto;
    public Miejsce(){
        this.id = nextId;
        nextId++;
        stan = false;
        kto = "Wolne";
    }
    public int getId(){return id;}
    public boolean getStan(){return stan;}
    public String getKto(){return kto;}

    public void setUprzywilejownie(boolean uprzywilejowanie){
        this.uprzywilejownaie = uprzywilejownaie;
    }
    public void wjazd(String dane){
        stan = true;
        kto = dane;
    }
    public void wyjazd(String dane){
        stan = false;
        kto = "Wolne";
    }
    public String toString(){
        return "Id: " + id + " || " + "Stan: " + stan + " || " + "Uprzywilejowanie: " + uprzywilejownaie + " || " + "Kto: " + kto;
    }
    static{
        nextId = 1;
    }
}

