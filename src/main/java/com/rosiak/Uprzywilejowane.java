package com.rosiak;

/**
 * Created by Rosiak on 22.01.2017.
 */
public final class Uprzywilejowane extends Zwykle {
    public Uprzywilejowane(){
        super.setUprzywilejownie(true);
    }
    public final void wj(){
        System.out.println( "Zajeto uprzywilejowane ");
    }
    public final  void wy(){
        System.out.println( "Zwolniono uprzywilejowane");
    }
}
